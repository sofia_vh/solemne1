<%-- 
    Document   : inex
    Created on : 28-abr-2021, 1:04:52
    Author     : sofia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/style.css"> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    
    <body>
        <div class="d-flex justify-content-center align-items-center" style="background-image: url('https://img.freepik.com/vector-gratis/fondo-blanco-minimo-hexagonos_79603-1452.jpg?size=626&ext=jpg'); height: 100vh;">

            <form name="form1" action="CalculadoraController" method="POST">
                <h2 align="center">Calculadora de Interés Simple</h2>

                <label for="nombre">Capital :</label>

                <input type="text" name="capital" id="monto" placeholder="Ingresa un monto"/>

                <label for="apellidos">Taza de Interés :</label>

                <input type="text" name="tinteres" id="interes" placeholder="%">

                <label for="email">Años :</label>

                <input type="text" name="years" id="years" placeholder="nº de años">
                
                <button type="submit" name="enviar" value="calcular" >CALCULAR</button>
               
            </form>

        </div>
    </body>
</html>
