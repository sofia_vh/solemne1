<%-- 
    Document   : respuesta
    Created on : 28-abr-2021, 1:05:34
    Author     : sofia
--%>
<%@page import="cl.modelo.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    Calculadora cal = (Calculadora) request.getAttribute("Calculadora");
%>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
       <body>
        <div class="d-flex justify-content-center align-items-center" style="background-image: url('https://img.freepik.com/vector-gratis/fondo-blanco-minimo-hexagonos_79603-1452.jpg?size=626&ext=jpg'); height: 500px;">
        <h1 align="center">Interés Simple de :<%=cal.getInsimple()%></h1>
        </div>
    </body>
    </body>
</html>
