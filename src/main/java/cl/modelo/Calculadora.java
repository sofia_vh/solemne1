/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.modelo;

/**
 *
 * @author sofia
 */
public class Calculadora {
    private double insimple;
    private int capital;
    private double tinteres;
    private int years;  
    /**
     * @return the insimple
     */
    public double getInsimple() {
        return insimple;
    }

    /**
     * @param insimple the insimple to set
     */
    public void setInsimple(double insimple) {
        this.insimple = insimple;
    }

    /**
     * @return the capital
     */
    public int getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(int capital) {
        this.capital = capital;
    }

    /**
     * @return the tinteres
     */
    public double getTinteres() {
        return tinteres;
    }

    /**
     * @param tinteres the tinteres to set
     */
    public void setTinteres(double tinteres) {
        this.tinteres = tinteres;
    }

    /**
     * @return the years
     */
    public int getYears() {
        return years;
    }

    /**
     * @param years the years to set
     */
    public void setYears(int years) {
        this.years = years;
    }
    public void interesSimple(){
        this.insimple= this.getCapital() * (this.getTinteres() /100) * this.getYears();
    }
}
